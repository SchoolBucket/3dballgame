﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureChest : MonoBehaviour {
    [SerializeField] private float distanceToPlayer;
    [SerializeField] private float collectDistance = 2f;
    [SerializeField] GameObject[] rewards;
    [SerializeField] private Transform spawnpoint;
    Transform player;
    public bool closeEnough, activated, rewarding;
    [SerializeField] private float deactivateTime;
	// Use this for initialization
	void Start () {
        player = GameObject.Find("player").transform;
	}
	void SpawnRewards(GameObject reward)
    {
        if (!rewarding)
            Instantiate(reward, spawnpoint.position, spawnpoint.rotation);
    }
	// Update is called once per frame
	void Update () {

        distanceToPlayer = Vector3.Distance(transform.position, player.position);
        Debug.Log(gameObject.name + " " + distanceToPlayer);
        if (distanceToPlayer <= collectDistance)
            closeEnough = true;
        else
            closeEnough = false;
        GameManager.current.SetInteractMessageActive(closeEnough);
        if (closeEnough)
        {
            Player_Controller.current.ActivateTreasureChest(this);
        }
        if (activated)
        {
            GameManager.current.SetInteractMessageActive(false);
            SpawnRewards(rewards[0]);
            rewarding = true;
            StartCoroutine("TurnOffOVerTime");
        }
            
	}
    IEnumerator TurnOffOVerTime()
    {
        yield return new WaitForSeconds(deactivateTime);
        Destroy(this);
    }
}
