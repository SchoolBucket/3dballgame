﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameManager : MonoBehaviour {
    private int currentScore;
    public static GameManager current = null;
    public Text scoreText, winText;
    public Transform pickups;
    public int resetTime, totalPoints;
    public GameObject miniMap;
    public bool miniMapVisible;
    public Animator anim;
    public bool shopVisible;
    private AudioSource swoosh;
    public float swooshWaitSeconds;
    public int currentCoinAmount;
private void Awake()
    {
        if (current != null)
        {
            Destroy(this);
        }
        current = this;
    }
    // Use this for initialization
    void Start () {
        currentScore = 0;
        scoreText.text = winText.text = "";
        totalPoints = pickups.childCount;
        miniMapVisible = false;
        swoosh = GetComponent<AudioSource>();
        currentCoinAmount = 0;
	}

    // Update is called once per frame
    void Update() {
        Debug.Log(currentScore);
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (miniMapVisible == true)
                miniMapVisible = false;
            else
                miniMapVisible = true;
            miniMap.SetActive(miniMapVisible);
        }
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (shopVisible == true)
                shopVisible = false;
            else
            {
                shopVisible = true;
                StartCoroutine("SwooshWait");

            }
            anim.SetBool("Active", shopVisible);
        }
        scoreText.text = "score: " + currentScore;
        if (currentScore == totalPoints){ 
            GameWin();
        }

	}
    public void AddScore(int score) {
        currentScore += score;

    }
    public void SetInteractMessageActive(bool active)
    {
        if (active)
            winText.text = "Press 'E' To Open";
        else
            winText.text = "";
    }
    void GameWin()
    {
        winText.text = "You L0s3 PL4Y 4G41N";
        StartCoroutine("ResetGame");
    }
    IEnumerator ResetGame ()
    {
        yield return new WaitForSeconds(resetTime);
        Application.LoadLevel(0);
    }
    IEnumerator SwooshWait()
    {
        yield return new WaitForSeconds(swooshWaitSeconds);
        swoosh.Play();
    }
    public void AddCoin (int coin)
    {
        currentCoinAmount += coin;
    }
    public void RemoveCoin(int coin)
    {
        currentCoinAmount -= coin;
    }
}
