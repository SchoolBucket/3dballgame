﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour {
    public int speedCost;
    public void BuySpeed ()
    {
        if (GameManager.current.currentCoinAmount >= speedCost)
        {
            Player_Controller.current.superSpeed = true;
            GameManager.current.RemoveCoin(speedCost);
        }
    }
}
        
