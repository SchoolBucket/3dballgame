﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public int coinValue;
    public float speedCost = 1;
    private void OnTriggerEnter(Collider other)
    {
        GameManager.current.AddCoin(coinValue);
        Destroy(this.gameObject);
    }
}
