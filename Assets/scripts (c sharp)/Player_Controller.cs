﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour {
    Rigidbody rb;
    float speed = 10.0f;
    // Use this for initialization
    public bool superSpeed;
    public float superSpeedTimerLength;
    float superSpeedTimer;
    public static Player_Controller current = null;
    private void Awake()
    {
        if (current != null)
        {
            Destroy(this);
        }
        current = this;
    }
    void Start () {
        rb = GetComponent<Rigidbody>();
        superSpeedTimer = superSpeedTimerLength;
	}


    // Update is called once per frame
    void Update()
    {
        if(superSpeed == true && superSpeedTimer > 0.0f)
        {
            superSpeedTimer -= Time.deltaTime;
        }
        else if(superSpeedTimer <=.0f)
        {
            superSpeed = false;
            superSpeedTimer = superSpeedTimerLength;
        }
    }
    void FixedUpdate ()
    {
		/*if(Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(transform.up * speed);
        }*/
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVeritcal = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVeritcal);
        if (superSpeed == true)
            rb.AddForce(movement * speed * 4);
        else
            rb.AddForce(movement * speed);
    }
    public void ActivateTreasureChest(TreasureChest treasure)
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            treasure.activated = true;
        }
    }
}
